import os
import logging
import sys
import connexion

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
MODULE_DIR = os.path.join(ROOT_DIR, "src")
sys.path.insert(0, MODULE_DIR)

sh = logging.StreamHandler()
logging.basicConfig(format='%(asctime)s |%(name)s|%(levelname)s|%(message)s',
                    level=logging.INFO,
                    handlers=[sh])

connx_app = connexion.App(__name__, specification_dir='./')
connx_app.add_api('server_api.yaml')
app = connx_app.app
app.url_map.strict_slashes = False


app.config['MONGODB_DB'] = os.environ.get('MONGODB_DB')
app.config['MONGODB_HOST'] = os.environ.get('MONGODB_HOST')
app.config['MONGODB_PORT'] = int(os.environ.get('MONGODB_PORT'))

if os.environ.get('MONGODB_USERNAME'):
    app.config['MONGODB_USERNAME'] = os.environ.get('MONGODB_USERNAME')
    app.config['MONGODB_PASSWORD'] = os.environ.get('MONGODB_PASSWORD')

from models.db import db
db.init_app(app)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)
