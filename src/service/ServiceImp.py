
from models.Product import Product
from models.ProductUpdateLog import ProductUpdateLog
from models.ProductTrack import ProductTrack
from models.AlertRule import AlertRule
from models.User import User
from lib.exceptions import ProductNotFound, ProductExists
import threading
from datetime import datetime


class ServiceImp:

	@staticmethod
	def save_product(product):

		try:
			model = Product.objects(sku=product['sku'], deleted=False).first()
		except Product.DoesNotExist:
			model = None

		if model is None:
			product_saved = Product(**product).save()
		else:
			raise ProductExists

		return product_saved.to_json()

	@staticmethod
	def update_product(product):

		model = ServiceImp.get_product(sku=product['sku'])

		if model:
			updated = False
			alerts = []

			# Iterating product keys
			for key in product.keys():

				if product[key] != model[key] and key != 'sku':
					model[key] = product[key]
					updated = True

					ServiceImp.verify_alert(key, alerts)

			if updated:
				model.last_modification_at = datetime.utcnow()
				model.save()

				if len(alerts) > 0:
					t = threading.Thread(target=ServiceImp.notify_update, args=(alerts, model))
					t.start()

				return 1
			else:
				return 0

	# Verify if update must be notified
	@staticmethod
	def verify_alert(key, alerts):
		alert_rule = AlertRule.get_by_entity_field(AlertRule.PRODUCT, key)

		if alert_rule is not None:
			alerts.append(alert_rule)

	# Notify product updates
	@staticmethod
	def notify_update(alerts, product):

		users = User.get_users_except(product.last_modification_by)

		if users and len(users) > 0:

			# Saving update log and notify via email (post_save update_log)
			update_log_json = {
				'username': product.last_modification_by,
				'sku': product.sku,
				'fields': list(map(lambda alert: alert.field, alerts))
			}

			update_log = ProductUpdateLog(**update_log_json)
			update_log.save(signal_kwargs={"users": list(users)})

	@staticmethod
	def delete_product(sku):

		model = ServiceImp.get_product(sku)

		if model:
			model.deleted = True
			model.last_modification_at = datetime.utcnow()
			model.save()

			# Verify if delete must be notified
			alerts = []
			ServiceImp.verify_alert("deleted", alerts)

			if len(alerts) > 0:
				t = threading.Thread(target=ServiceImp.notify_update, args=(alerts, model))
				t.start()

		return 1

	@staticmethod
	def get_products():

		return Product.objects(deleted=False).only('sku', 'name', 'price', 'brand', 'last_modification_by').to_json()

	@staticmethod
	def get_product(sku, track=False):

		try:
			model = Product.objects(sku=sku, deleted=False).first()

			if model:

				# Saving product queried
				if track:
					ProductTrack(**{"sku": sku}).save()

				return model
			else:
				raise ProductNotFound
		except Product.DoesNotExist:
			raise ProductNotFound
