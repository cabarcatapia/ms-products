from service.ServiceImp import ServiceImp
from lib.exceptions import ProductException, Message

from flask import request, Response
from bson.json_util import dumps
import json


class ServiceFacade:

	@staticmethod
	def save_product():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.save_product(request.json)
			kwargs['response'] = json.dumps(response)
			kwargs['status'] = 201
		except ProductException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def update_product():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.update_product(request.json)
			kwargs['response'] = json.dumps(response)
			kwargs['status'] = 201
		except ProductException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def delete_product():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.delete_product(request.args.get('sku'))
			kwargs['response'] = json.dumps(response)
			kwargs['status'] = 200
		except ProductException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def get_products():

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.get_products()
			kwargs['response'] = dumps(json.loads(response))
			kwargs['status'] = 200
		except ProductException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)

	@staticmethod
	def get_product(sku):

		kwargs = {
			'mimetype': "application/json"
		}
		try:
			response = ServiceImp.get_product(sku, True)
			kwargs['response'] = json.dumps(response.to_json())
			kwargs['status'] = 200
		except ProductException as ex:
			kwargs['response'] = Message(str(ex), Message.INFO).to_json()
			kwargs['status'] = ex.status_code

		return Response(**kwargs)
