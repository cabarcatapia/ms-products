from models.db import db
from mongoengine_goodjson import Document
from datetime import datetime


# Track when a product is queried by an anonymous user
class ProductTrack(Document):

    sku = db.StringField()
    datetime = db.DateTimeField(default=datetime.utcnow())

    meta = {
        'collection': 'product_track',
        'strict': False
    }
