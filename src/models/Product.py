from models.db import db
from mongoengine_goodjson import Document
from datetime import datetime


# Product model
class Product(Document):

    sku = db.StringField()
    name = db.StringField()
    price = db.FloatField()
    brand = db.StringField()
    last_modification_by = db.StringField()
    last_modification_at = db.DateTimeField(default=datetime.utcnow())
    deleted = db.BooleanField(default=False)

    def to_json(self):
        return {
            "sku": self.sku,
            "name": self.name,
            "price": self.price,
            "brand": self.brand,
            "last_modification_by": self.last_modification_by
        }

    meta = {
        'collection': 'products',
        'strict': False
    }
