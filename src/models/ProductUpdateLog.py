from models.db import db
from mongoengine_goodjson import Document
from mongoengine import signals
from lib.utils import Utils
from datetime import datetime


# Log when a product is updated by an admin user
class ProductUpdateLog(Document):

    username = db.StringField()
    sku = db.StringField()
    fields = db.ListField()
    datetime = db.DateTimeField(default=datetime.utcnow())

    meta = {
        'collection': 'product_update_log',
        'strict': False
    }

    @classmethod
    def post_save(cls, sender, document, **kwargs):

        if kwargs and "users" in kwargs:
            message = "The user {}, has updated product SKU {}. The fields updated was: {}".format(document.username, document.sku, ', '.join(document.fields))

            for user in kwargs.get("users"):
                Utils.send_email(user.email, 'SKU updated', message)


signals.post_save.connect(ProductUpdateLog.post_save, sender=ProductUpdateLog)
