from mongoengine import Document

from models.db import db


# An alert rule indicate if a field of an entity must sent a notification if was updated
class AlertRule(Document):

    PRODUCT = "product"
    USER = "user"

    entity = db.StringField()
    field = db.StringField()

    meta = {
        'collection': "alert_rules",
        'strict': False
    }

    @staticmethod
    def get_by_entity_field(entity, field):
        return AlertRule.objects(entity=entity, field=field).first()
