from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import os


class Utils:

    @staticmethod
    def send_email(to, subject, message):
        msg = MIMEMultipart()

        password = os.environ.get('SMTP_PASS')
        msg['From'] = os.environ.get('SMTP_FROM')
        msg['To'] = to
        msg['Subject'] = subject
        msg.attach(MIMEText(message, 'plain'))

        server = smtplib.SMTP(os.environ.get('SMTP_CONFIG'))
        server.starttls()

        server.login(msg['From'], password)
        server.sendmail(msg['From'], msg['To'], msg.as_string())

        server.quit()

