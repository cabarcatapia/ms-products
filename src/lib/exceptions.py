from http import HTTPStatus
import json


class ProductException(Exception):
    """Base class for other exceptions"""
    def __init__(self, message, status_code):
        super(ProductException, self).__init__(message)
        self.status_code = status_code


class ProductNotFound(ProductException):
    """Raised when a product is not found"""
    def __init__(self):
        super(ProductNotFound, self).__init__(
            "Product not found", HTTPStatus.NOT_FOUND
        )


class ProductExists(ProductException):
    """Raised when a product already exists"""
    def __init__(self):
        super(ProductExists, self).__init__(
            "Product already exists", HTTPStatus.BAD_REQUEST
        )


class Message:

    INFO = 'info'
    ERROR = 'error'

    def __init__(self, message, level):
        self.message = message
        self.level = level

    def to_json(self):
        return json.dumps(self.__dict__)