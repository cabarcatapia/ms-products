

# BUILD / RUN
### 1. Build / Run with docker-compose

**To install docker-compose see**

   [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

**Build / run**

From base path of the project

        sudo docker-compose up --build
        
Microservice will avalaible on

        http://locahost:8000/products/api/v1/
        
**Config**

Develop configuration, like as database connection or SMTP configuration, are located on file .env

**Base data**

Base data for config alert rules (notification about product fields update), are in file **data/alert_rules.json**. This data must be saved on collection **"alert_rules"**.
        
### 2. API Documentation
Documentation is avalaible on
        
   [http://locahost:8100/users/api/v1/ui/](http://locahost:8100/users/api/v1/ui/)

### 3. Future work
The work not included in this version is the following:

    - Services security with user token
    - User identification by token for product update alert
    - Template for email